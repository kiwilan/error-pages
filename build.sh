#!/bin/bash

# Step 1: Compile CSS
npx tailwindcss -i ./css/style.css -o ./dist/style.css -wm

# Step 2: Inject compiled CSS into index.html
COMPILED_CSS=$(cat ./dist/style.css)

# Use sed to find <style> tag in index.html and replace its content
sed -i.bak '/<style>/,/<\/style>/{/<style>/!{/<\/style>/!d}}' ./index.html
sed -i "/<style>/r /dev/stdin" ./index.html <<<"$COMPILED_CSS"

echo "CSS successfully embedded in index.html"
